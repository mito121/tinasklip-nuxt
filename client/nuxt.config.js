export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "tinasklip",
    htmlAttrs: {
      lang: "da",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    // link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.png" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~assets/css/main.css"],

  // script: [
  //   /* Facebook iframe */
  //   {
  //     src: "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v13.0&appId=674507512915560&autoLogAppEvents=1",
  //     defer: true,
  //   },
  // ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vue-agile",
    {
      src: "~/plugins/slide-menu",
      ssr: false,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  // components: true,
  components: ["~/components", "~/partials"],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
    // "@nuxtjs/fontawesome",
  ],

  // fontawesome: {
  //   icons: {
  //     solid: true,
  //   },
  // },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/auth", "vue-sweetalert2/nuxt"],

  axios: {
    // baseURL: "http://localhost:3001",
    baseURL: "https://tinasklip-api.herokuapp.com/",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ["vue-agile", "vue-cool-lightbox"],
  },

  styleResources: {
    scss: ["~assets/scss/main.scss", "~assets/scss/variables.scss"],
  },

  pageTransition: {
    name: "layout",
  },

  auth: {
    // cookie: false,
    strategies: {
      local: {
        endpoints: {
          login: { urL: "/auth/login", method: "post", propertyName: "token" },
          logout: { urL: "/auth/logout", method: "delete" },
          user: { urL: "/auth/user", method: "get", propertyName: "user" },
        },
      },
    },
    redirect: {
      login: "/login",
      logout: "/",
      callback: "/login",
      home: "/",
    },
  },
};
