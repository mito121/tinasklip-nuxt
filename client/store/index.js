export const state = () => ({
  windowWidth: null,
});

// Actions
export const actions = {
  setWindowWidth: async (context, payload) => {
    context.commit("setWindowWidth", payload);
  },
};
// Mutations
export const mutations = {
  setWindowWidth(state, payload) {
    state.windowWidth = payload;
  },
};

// Getters
export const getters = {
  getWindowWidth(state) {
    return state.windowWidth;
  },
};
