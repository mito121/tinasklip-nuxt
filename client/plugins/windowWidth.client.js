export default (context, inject) => {
  /* Debounce helper function */
  // function debounce(cb, delay = 500) {
  //   let timeout;

  //   return (...args) => {
  //     clearTimeout(timeout);
  //     timeout = setTimeout(() => {
  //       cb(...args);
  //     }, delay);
  //   };
  // }

  /* Throttle helper function */
  function throttle(cb, delay = 500) {
    let wait = false;
    let waitingArgs = null;

    const timeout = () => {
      if (waitingArgs == null) {
        wait = false;
      } else {
        cb(...waitingArgs);
        waitingArgs = null;
        setTimeout(timeout, delay);
      }
    };

    return (...args) => {
      if (wait) {
        waitingArgs = args;
        return;
      }

      cb(...args);
      wait = true;

      setTimeout(timeout, delay);
    };
  }

  const updateWindowWidth = throttle(() => {
    context.store.dispatch("setWindowWidth", window.innerWidth);
    console.log("w", window.innerWidth);
  });

  /* Set initial window width */
  context.store.dispatch("setWindowWidth", window.innerWidth);

  /* Set window width on resize */
  window.addEventListener("resize", updateWindowWidth);
};
